import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:lecture_practice_6/provider/data_provider.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DataProvider>(
      create: (context) => DataProvider(),
      builder: (context, child) => MaterialApp(
        home: MyCounter(),
      ),
    );
  }
}

class MyCounter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var function = Provider.of<DataProvider>(context, listen: false);
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ColoredBox(
            color: Colors.deepOrange,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Selector<DataProvider, int>(
                  selector: (context, counter) => counter.firstCounter,
                  builder: (context, value, child) {
                    log('First was updated');
                    return Text(
                      '$value',
                      style: TextStyle(fontSize: 40),
                    );
                  },
                ),
                Selector<DataProvider, int>(
                  selector: (context, counter) => counter.secondCounter,
                  builder: (context, value, child) {
                    log('Second was updated');
                    return Text(
                      '$value',
                      style: TextStyle(fontSize: 40),
                    );
                  },
                ),
              ],
            ),
          ),
          ColoredBox(
            color: Colors.lightBlue,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  icon: Icon(Icons.plus_one),
                  iconSize: 40,
                  onPressed: () {
                    function.increment();
                  },
                ),
                IconButton(
                  icon: Icon(Icons.exposure_minus_1),
                  iconSize: 40,
                  onPressed: () {
                    function.decrement();
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

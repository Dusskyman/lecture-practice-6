import 'dart:math';

import 'package:flutter/material.dart';

class DataProvider with ChangeNotifier {
  int firstCounter = 0;
  int secondCounter = 0;
  bool choise;

  void increment() {
    choise = Random().nextBool();
    if (choise) {
      firstCounter++;
    } else {
      secondCounter++;
    }
    notifyListeners();
  }

  void decrement() {
    choise = Random().nextBool();
    if (choise) {
      firstCounter--;
    } else {
      secondCounter--;
    }
    notifyListeners();
  }
}
